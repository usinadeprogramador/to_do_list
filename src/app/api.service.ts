import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Categoria } from './categorias/categoria';
import { List } from './lists/list';
import { Item } from './itens/item';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://5df19c009df6fb00142bdb5c.mockapi.io/";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getCategorias (endpoint: string): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(`${apiUrl}/${endpoint}`)
      .pipe(
        tap(heroes => console.log('fetched products')),
        catchError(this.handleError('getCategorias', []))
      );
  }

  getCategoria(id: number): Observable<Categoria> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Categoria>(url).pipe(
      tap(_ => console.log(`fetched product id=${id}`)),
      catchError(this.handleError<Categoria>(`getCategoria id=${id}`))
    );
  }

  addCategoria (categoria): Observable<Categoria> {
    return this.http.post<Categoria>(apiUrl, categoria, httpOptions).pipe(
      tap((categoria: Categoria) => console.log(`added categoria w/ id=${categoria.id}`)),
      catchError(this.handleError<Categoria>('addCategoria'))
    );
  }

  updateCategoria (id, categoria): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, categoria, httpOptions).pipe(
      tap(_ => console.log(`updated categoria id=${id}`)),
      catchError(this.handleError<any>('updateCategoria'))
    );
  }

  deleteCategoria (id): Observable<Categoria> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Categoria>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted product id=${id}`)),
      catchError(this.handleError<Categoria>('deleteCategoria'))
    );
  }

  getLists (endpoint: string): Observable<List[]> {
    return this.http.get<List[]>(`${apiUrl}/${endpoint}`)
      .pipe(
        tap(heroes => console.log('fetched List')),
        catchError(this.handleError('getList', []))
      );
  }

  getList(id: number): Observable<List> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<List>(url).pipe(
      tap(_ => console.log(`fetched list id=${id}`)),
      catchError(this.handleError<List>(`getList id=${id}`))
    );
  }

  addList (endpoint: string, list): Observable<List> {
    return this.http.post<List>(`${apiUrl}/${endpoint}`, list, httpOptions).pipe(
      tap((list: List) => console.log(`added list w/ id=${list.id}`)),
      catchError(this.handleError<List>('addList'))
    );
  }

  getItem(id: number): Observable<Item> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Item>(url).pipe(
      tap(_ => console.log(`fetched item id=${id}`)),
      catchError(this.handleError<Item>(`getItem id=${id}`))
    );
  }

  addItem (endpoint: string, item): Observable<Item> {
    return this.http.post<Item>(`${apiUrl}/${endpoint}`, item, httpOptions).pipe(tap(
      (list: Item) => console.log(`added list w/ id=${list.id}`)
    ),
      catchError(this.handleError<Item>('addItem'))
    );
  }

  getItens (endpoint: string): Observable<Item[]> {
    return this.http.get<Item[]>(`${apiUrl}/${endpoint}`)
      .pipe(
        tap(heroes => console.log('fetched Item')),
        catchError(this.handleError('getItens', []))
      );
  }

  updateItem (endpoint: string, id, item): Observable<any> {
    const url = `${apiUrl}/${endpoint}/${id}`;
    return this.http.put(url, item, httpOptions).pipe(
      tap(_ => console.log(`updated item id=${id}`)),
      catchError(this.handleError<any>('updateItem'))
    );
  }
}
