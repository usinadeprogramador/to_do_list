import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriasComponent } from './categorias/categorias.component';
import { ListsComponent } from './lists/lists.component';
import { ItensComponent } from './itens/itens.component';

const routes: Routes = [
  { path: '', component: CategoriasComponent },
  { path: 'lists', component: ListsComponent },
  { path: 'itens', component: ItensComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }




