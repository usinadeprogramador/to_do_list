import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { ApiService } from '../api.service';
import { List } from '../lists/list';
import { Item } from './item';

@Component({
  selector: 'app-itens',
  templateUrl: './itens.component.html',
  styleUrls: ['./itens.component.sass']
})
export class ItensComponent implements OnInit {

  form: FormGroup;
  lists: List[] = [];
  itens: Item[] = [];
  displayedColumns: string[] = [ 'id', 'name', 'lista', 'concluido','acao'];
  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      listID: [''],
      name: ""
    });
  }

  enviarDados() {
    console.log(this.form.value);
    this.api.addItem('itens', this.form.value).subscribe(
      res => {
        this.getItens();
      }, err => {
        console.log(err);
      }
    );
  }

  getItens(){
    this.api.getItens('itens').subscribe(
      res => {
        this.itens = res;
        this.itens.forEach((obj) => {
          let listSelected;
          listSelected = this.lists.find(list => list.id == obj.listID);
          obj.lista = listSelected ? listSelected.name : '-';
          obj.status = (obj.done) ? 'Concluído': 'pendente';
        });
      }, err => {
        console.log(err);
      }
    );
  }

  setItem(item){
    this.api.updateItem('itens', item.id, item).subscribe(
      res => {
        this.getItens();
      }, err => {
        console.log(err);
      }
    );
  }

  getStatus(status){
    (status) ? 'Concluído': 'pendente';
  }

  setDone(item){
    item.done = true;
    this.setItem(item);
  }

  ngOnInit() {
    this.api.getLists('lists').subscribe(
      res => {
        this.lists = res;
        console.log(this.lists);
        this.getItens();
      }, err => {
        console.log(err);
      }
    );
  }
}
