export class Item {
    id: number;
    name: string;
    listID: number;
    lista: string;
    done: boolean;
    status: string;
    updated_at: Date;
}
