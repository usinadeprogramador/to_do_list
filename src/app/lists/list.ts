export class List {
    id: number;
    name: string;
    categoriaID: number;
    categoria: string;
    updated_at: Date;
  }
