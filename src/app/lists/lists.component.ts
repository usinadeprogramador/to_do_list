import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { ApiService } from '../api.service';
import { Categoria } from '../categorias/categoria';
import { List } from './list';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {

  form: FormGroup;
  categorias: Categoria[] = [];
  lists: List[] = [];
  displayedColumns: string[] = [ 'id', 'name', 'categoria'];
  constructor(private api: ApiService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      categoriaID: [''],
      name: ""
    });
  }


  enviarDados() {
    console.log(this.form.value);
    this.api.addList('lists', this.form.value).subscribe(
      res => {
        this.getLists();
        console.log('adicionou');
      }, err => {
        console.log(err);
      }
    );
  }

  getLists(){
    this.api.getLists('lists').subscribe(
      res => {
        this.lists = res;
        this.lists.forEach((obj) => {
          let categoriaSelected;
          categoriaSelected = this.categorias.find(categoria => categoria.id == obj.categoriaID);
          obj.categoria = categoriaSelected ? categoriaSelected.name : '-';
          console.log(obj.categoriaID);
        });


      }, err => {
        console.log(err);
      }
    );
  }


  ngOnInit() {
    this.api.getCategorias('categories').subscribe(
      res => {
        this.categorias = res;
        console.log(this.categorias);
        this.getLists();
      }, err => {
        console.log(err);
      }
    );
  }

}
